﻿using BLL.ViewModel;
using DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Manager
    {
        Data data;
        public Manager()
        {
            data = new Data();
            var alma = data.GetActiveItems().ToList();
        }

        public void SaveConnectionType(ConnectionTypeVM connectionType)
        {
            data.SaveConnectionType(connectionType.VMToDB(connectionType));
            data.SaveConnectionDataList(connectionType.ConnectionDatas.Select(x => x.VMToDB(x)).ToList(), connectionType.TypeId);
        }

        public List<ConnectionTypeVM> GetConnectionTypes()
        {
            var connectionType = data.GetConnectionTypes().Select(x => new ConnectionTypeVM
            {
                TypeId = x.TypeId,
                TypeName = x.TypeName
            }).ToList();

            foreach (var item in connectionType)
            {
                item.ConnectionDatas = this.GetConnectionTypeProperties(item.TypeId);
            }
            return connectionType;
        }

        public List<ConnectionDataVM> GetConnectionTypeProperties(string connectionTypeId)
        {
            return data.GetConnectionTypeProperties().Where(x => x.ConnectionTypeId == connectionTypeId).Select(x => new ConnectionDataVM()
            {
                ConnectionTypeId = x.ConnectionTypeId,
                ConnectionData = x.ConnectionData1,
                ConnectionDataName = x.ConnectionDataName
            }).ToList();
        }

        public void DeleteConnectionType(string connectionTypeId)
        {
            data.DeleteConnectionType(connectionTypeId);
        }

        public void SaveConnectionData(ConnectionDataVM connectionDataVM)
        {
            data.SaveConnectionData(connectionDataVM.VMToDB(connectionDataVM));
        }

        public List<ObjectVM> GetAllObjects()
        {
            List<ObjectVM> objects = new List<ObjectVM>();

            //ActiveItems
            objects.AddRange(data.GetActiveItems().Select(x => new ObjectVM()
            {
                DeviceId = x.DeviceId,
                DeviceName = x.DeviceName,
                DisplayName = x.DeviceName + ": from ActiveItems"
            }));

            //PassiveItems
            objects.AddRange(data.GetPassiveItems().Select(x => new ObjectVM()
            {
                DeviceId = x.DeviceId,
                DeviceName = x.DeviceName,
                DisplayName = x.DeviceName + ": from PassiveItems"
            }));

            //ActivePorts
            objects.AddRange(data.GetActivePorts().Select(x => new ObjectVM()
            {
                DeviceId = x.PortId,
                DeviceName = x.PortName,
                DisplayName = x.PortName + ": from ActivePorts"
            }));

            //PassivePorts
            objects.AddRange(data.GetPassivePorts().Select(x => new ObjectVM()
            {
                DeviceId = x.PortId,
                DeviceName = x.PortId + " PassivePort",
                DisplayName = x.PortId + " PassivePort" + ": from PassivePorts"
            }));

            //Locations
            objects.AddRange(data.GetLocations().Select(x => new ObjectVM()
            {
                DeviceId = x.LocationId,
                DeviceName = x.Name,
                DisplayName = x.Name + ": from Locations"
            }));

            //Connectors
            objects.AddRange(data.GetWallConnectors().Select(x => new ObjectVM()
            {
                DeviceId = x.ConnectorId,
                DeviceName = x.Name,
                DisplayName = x.Name + ": from Connectors"
            }));

            return objects;
        }

        public void SaveConnection(string object1Id, string object2Id, string connectionTypeId)
        {
            data.SaveConnection(object1Id, object2Id, connectionTypeId);
        }

        public List<ConnectionVM> GetConnectionsById(string objectId)
        {
            return data.GetObjectConnections().Where(x => x.ValidUntil == null && (x.Object1 == objectId || x.Object2 == objectId)).Select(x=> new ConnectionVM() {
                Object1 = x.Object1,
                Object2 = x.Object2,
                ConnectionTypeId = x.ConnectionTypeId,
                Date = x.Date,
                User = x.UserLogin
            }).Distinct().ToList();
        }

        public List<ConnectionVM> GetConnectionsForDisplay()
        {
            var objects = GetAllObjects();
            var connectionTypes = GetConnectionTypes();
            var connectionList = data.GetObjectConnections().Where(x => x.ValidUntil == null).Select(x => new ConnectionVM() { Object1 = x.Object1, Object2 = x.Object2, ConnectionTypeId = x.ConnectionTypeId, User = x.UserLogin, Date = x.Date, }).ToList();
            foreach(ConnectionVM obj in connectionList)
            {
                obj.Object1Display = objects.Where(x => x.DeviceId == obj.Object1).Select(y => y.DisplayName).FirstOrDefault();
                obj.Object2Display = objects.Where(x => x.DeviceId == obj.Object2).Select(y => y.DisplayName).FirstOrDefault();
                obj.ConnectionTypeIdDisplay = connectionTypes.Where(x => x.TypeId == obj.ConnectionTypeId).Select(y => y.TypeName).FirstOrDefault();
            };
            return connectionList;
        }

        public void DeleteConnection(string object1Id, string object2Id, string connectionTypeId)
        {
            data.DeleteConnection(object1Id, object2Id, connectionTypeId);
        }
    }
}
