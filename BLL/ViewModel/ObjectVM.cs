﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ViewModel
{
    public class ObjectVM
    {
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DisplayName { get; set; }

        public string ConnectionTypeId { get; set; }
    }
}
