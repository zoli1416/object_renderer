﻿using DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ViewModel
{
    public class ConnectionDataVM
    {
        public string ConnectionTypeId { get; set; }
        public string ConnectionDataName { get; set; }
        public string ConnectionData { get; set; }

        public ConnectionData VMToDB(ConnectionDataVM VM)
        {
            ConnectionData connectionData = new ConnectionData();
            connectionData.ConnectionTypeId = VM.ConnectionTypeId;
            connectionData.ConnectionDataName = VM.ConnectionDataName;
            connectionData.ConnectionData1 = VM.ConnectionData;
            return connectionData;
        }
    }
}
