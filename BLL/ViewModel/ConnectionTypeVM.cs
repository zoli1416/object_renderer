﻿using DLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ViewModel
{
    public class ConnectionTypeVM
    {
        public string TypeId { get; set; }
        public string TypeName { get; set; }
        public List<ConnectionDataVM> ConnectionDatas { get; set; }

        public ConnectionType VMToDB(ConnectionTypeVM VM)
        {
            ConnectionType connectionType = new ConnectionType();
            connectionType.TypeId = VM.TypeId;
            connectionType.TypeName = VM.TypeName;
            return connectionType;
        }
    }
}
