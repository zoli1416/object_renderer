﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ViewModel
{
    public class ConnectionVM
    {
        public string Object1 { get; set; }
        public string Object2 { get; set; }
        public string ConnectionTypeId { get; set; }
        public string Object1Display { get; set; }
        public string Object2Display { get; set; }
        public string ConnectionTypeIdDisplay { get; set; }
        public DateTime? Date { get; set; }
        public string User { get; set; }
    }
}
