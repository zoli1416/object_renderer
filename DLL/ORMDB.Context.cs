﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DLL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ORMEntities : DbContext
    {
        public ORMEntities()
            : base("name=ORMEntities")
        {
            var ensureDLLIsCopied =
                System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ConnectionData> ConnectionData { get; set; }
        public virtual DbSet<ConnectionType> ConnectionType { get; set; }
        public virtual DbSet<ItemActive> ItemActive { get; set; }
        public virtual DbSet<ItemPassive> ItemPassive { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationType> LocationType { get; set; }
        public virtual DbSet<ObjectConnection> ObjectConnection { get; set; }
        public virtual DbSet<PortActive> PortActive { get; set; }
        public virtual DbSet<PortPassive> PortPassive { get; set; }
        public virtual DbSet<Symbol> Symbol { get; set; }
        public virtual DbSet<WallConnector> WallConnector { get; set; }
    }
}
