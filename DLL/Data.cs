﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL
{
    public class Data
    {
        ORMEntities db;

        public Data()
        {
            db = new ORMEntities();
        }
        public IQueryable<ItemActive> GetActiveItems()
        {
            return db.ItemActive;
        }

        public IQueryable<ItemPassive> GetPassiveItems()
        {
            return db.ItemPassive;
        }

        public IQueryable<PortActive> GetActivePorts()
        {
            return db.PortActive;
        }

        public IQueryable<PortPassive> GetPassivePorts()
        {
            return db.PortPassive;
        }

        public IQueryable<Symbol> GetSymbols()
        {
            return db.Symbol;
        }

        public IQueryable<Location> GetLocations()
        {
            return db.Location;
        }

        public IQueryable<LocationType> GetLocationTypes()
        {
            return db.LocationType;
        }

        public IQueryable<WallConnector> GetWallConnectors()
        {
            return db.WallConnector;
        }

        public IQueryable<ConnectionType> GetConnectionTypes()
        {
            return db.ConnectionType;
        }

        public IQueryable<ConnectionData> GetConnectionTypeProperties()
        {
            return db.ConnectionData;
        }
        public void SaveConnectionType(ConnectionType connectionType)
        {
            var connectionTypeInDb = db.ConnectionType.Where(x => x.TypeId == connectionType.TypeId).FirstOrDefault();
            if (connectionTypeInDb != null)
            {
                connectionTypeInDb.TypeName = connectionType.TypeName;
            }
            else
            {
                db.ConnectionType.Add(connectionType);
            }
            db.SaveChanges();
        }

        public void SaveConnectionDataList(List<ConnectionData> connectionDatas, string connectionTypeId)
        {
            db.ConnectionData.RemoveRange(db.ConnectionData.Where(x => x.ConnectionTypeId == connectionTypeId));
            db.ConnectionData.AddRange(connectionDatas);
            db.SaveChanges();
        }

        public void DeleteConnectionType(string connectionTypeId)
        {
            db.ConnectionData.RemoveRange(db.ConnectionData.Where(x => x.ConnectionTypeId == connectionTypeId));
            db.ConnectionType.RemoveRange(db.ConnectionType.Where(x => x.TypeId == connectionTypeId));
            db.SaveChanges();
        }

        public void SaveConnectionData(ConnectionData connectionData)
        {
            var connectionDataDB = db.ConnectionData.Where(x => x.ConnectionTypeId == connectionData.ConnectionTypeId && x.ConnectionDataName == connectionData.ConnectionDataName).FirstOrDefault();
            if (connectionDataDB != null)
            {
                connectionDataDB.ConnectionData1 = connectionData.ConnectionData1;
            }
            db.SaveChanges();
        }

        public void SaveConnection(string object1Id, string object2Id, string connectionTypeId)
        {
            var connectionDB = db.ObjectConnection.Where(x => x.Object1 == object1Id && x.Object2 == object2Id && x.ConnectionTypeId == connectionTypeId && x.ValidUntil == null).FirstOrDefault();
            if (connectionDB != null)
            {
                connectionDB.ValidUntil = DateTime.Now;

            }
            db.ObjectConnection.Add(new ObjectConnection()
            {
                Object1 = object1Id,
                Object2 = object2Id,
                ConnectionTypeId = connectionTypeId,
                UserLogin = Environment.UserName,
                Date = DateTime.Now.Date
            });
            db.SaveChanges();
        }

        public IQueryable<ObjectConnection> GetObjectConnections()
        {
            return db.ObjectConnection;
        }

        public void DeleteConnection(string object1Id, string object2Id, string connectionTypeId)
        {
            var connectionDB = db.ObjectConnection.Where(x => x.Object1 == object1Id && x.Object2 == object2Id && x.ConnectionTypeId == connectionTypeId && x.ValidUntil == null).FirstOrDefault();
            if (connectionDB != null)
            {
                connectionDB.ValidUntil = DateTime.Now;

            }
            db.SaveChanges();
        }
    }
}
