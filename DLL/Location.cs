//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Location
    {
        public long Id { get; set; }
        public string LocationId { get; set; }
        public string Name { get; set; }
        public Nullable<int> LocationTypeId { get; set; }
        public Nullable<long> ParentId { get; set; }
        public Nullable<int> SymbolId { get; set; }
        public string Description { get; set; }
    }
}
