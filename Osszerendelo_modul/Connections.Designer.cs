﻿namespace Osszerendelo_modul
{
    partial class Connections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Object1 = new System.Windows.Forms.ListBox();
            this.Object2 = new System.Windows.Forms.ListBox();
            this.ConnectionTypeList = new System.Windows.Forms.ListBox();
            this.ConnectionTypeProperties = new System.Windows.Forms.ListBox();
            this.CreateConnectionButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Object1ExistingConnections = new System.Windows.Forms.ListView();
            this.Object2ExistingConnections = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // Object1
            // 
            this.Object1.DisplayMember = "DisplayName";
            this.Object1.FormattingEnabled = true;
            this.Object1.Location = new System.Drawing.Point(12, 69);
            this.Object1.Name = "Object1";
            this.Object1.Size = new System.Drawing.Size(170, 212);
            this.Object1.TabIndex = 0;
            this.Object1.ValueMember = "DeviceId";
            this.Object1.SelectedIndexChanged += new System.EventHandler(this.Object1_SelectedIndexChanged);
            // 
            // Object2
            // 
            this.Object2.DisplayMember = "DisplayName";
            this.Object2.FormattingEnabled = true;
            this.Object2.Location = new System.Drawing.Point(199, 69);
            this.Object2.Name = "Object2";
            this.Object2.Size = new System.Drawing.Size(170, 212);
            this.Object2.TabIndex = 1;
            this.Object2.ValueMember = "DeviceId";
            this.Object2.SelectedIndexChanged += new System.EventHandler(this.Object2_SelectedIndexChanged);
            // 
            // ConnectionTypeList
            // 
            this.ConnectionTypeList.DisplayMember = "TypeName";
            this.ConnectionTypeList.FormattingEnabled = true;
            this.ConnectionTypeList.Location = new System.Drawing.Point(388, 69);
            this.ConnectionTypeList.Name = "ConnectionTypeList";
            this.ConnectionTypeList.Size = new System.Drawing.Size(170, 212);
            this.ConnectionTypeList.TabIndex = 2;
            this.ConnectionTypeList.ValueMember = "TypeId";
            this.ConnectionTypeList.SelectedIndexChanged += new System.EventHandler(this.ConnectionTypeList_SelectedIndexChanged);
            // 
            // ConnectionTypeProperties
            // 
            this.ConnectionTypeProperties.DisplayMember = "ConnectionDataName";
            this.ConnectionTypeProperties.FormattingEnabled = true;
            this.ConnectionTypeProperties.Location = new System.Drawing.Point(575, 69);
            this.ConnectionTypeProperties.Name = "ConnectionTypeProperties";
            this.ConnectionTypeProperties.Size = new System.Drawing.Size(170, 212);
            this.ConnectionTypeProperties.TabIndex = 3;
            this.ConnectionTypeProperties.DoubleClick += new System.EventHandler(this.ConnectionTypeProperties_DoubleClick);
            // 
            // CreateConnectionButton
            // 
            this.CreateConnectionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CreateConnectionButton.Location = new System.Drawing.Point(575, 415);
            this.CreateConnectionButton.Name = "CreateConnectionButton";
            this.CreateConnectionButton.Size = new System.Drawing.Size(268, 110);
            this.CreateConnectionButton.TabIndex = 6;
            this.CreateConnectionButton.Text = "Create Connection";
            this.CreateConnectionButton.UseVisualStyleBackColor = true;
            this.CreateConnectionButton.Click += new System.EventHandler(this.CreateConnectionButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(38, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "First Object";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(206, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "Second Object";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(383, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 25);
            this.label3.TabIndex = 9;
            this.label3.Text = "Connection Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(570, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(279, 25);
            this.label4.TabIndex = 10;
            this.label4.Text = "Connection Type Properties";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(12, 334);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(248, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "First Object Connections";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(271, 334);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(279, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "Second Object Connections";
            // 
            // Object1ExistingConnections
            // 
            this.Object1ExistingConnections.Location = new System.Drawing.Point(17, 362);
            this.Object1ExistingConnections.Name = "Object1ExistingConnections";
            this.Object1ExistingConnections.Size = new System.Drawing.Size(243, 212);
            this.Object1ExistingConnections.TabIndex = 13;
            this.Object1ExistingConnections.UseCompatibleStateImageBehavior = false;
            // 
            // Object2ExistingConnections
            // 
            this.Object2ExistingConnections.Location = new System.Drawing.Point(276, 362);
            this.Object2ExistingConnections.Name = "Object2ExistingConnections";
            this.Object2ExistingConnections.Size = new System.Drawing.Size(243, 212);
            this.Object2ExistingConnections.TabIndex = 14;
            this.Object2ExistingConnections.UseCompatibleStateImageBehavior = false;
            // 
            // Connections
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 645);
            this.Controls.Add(this.Object2ExistingConnections);
            this.Controls.Add(this.Object1ExistingConnections);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CreateConnectionButton);
            this.Controls.Add(this.ConnectionTypeProperties);
            this.Controls.Add(this.ConnectionTypeList);
            this.Controls.Add(this.Object2);
            this.Controls.Add(this.Object1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Connections";
            this.Text = "Connections";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Object1;
        private System.Windows.Forms.ListBox Object2;
        private System.Windows.Forms.ListBox ConnectionTypeList;
        private System.Windows.Forms.ListBox ConnectionTypeProperties;
        private System.Windows.Forms.Button CreateConnectionButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView Object1ExistingConnections;
        private System.Windows.Forms.ListView Object2ExistingConnections;
    }
}