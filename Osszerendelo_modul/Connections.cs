﻿using BLL;
using BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Osszerendelo_modul
{
    public partial class Connections : Form
    {
        Manager manager;
        List<ConnectionTypeVM> connectionTypes;
        List<ObjectVM> objects1;
        List<ObjectVM> objects2;
        public Connections(Manager manager)
        {
            InitializeComponent();
            this.manager = manager;

            //ConnectionType
            this.connectionTypes = manager.GetConnectionTypes();
            this.ConnectionTypeList.DataSource = this.connectionTypes;

            this.objects1 = new List<ObjectVM>();
            this.objects1.AddRange(manager.GetAllObjects());

            this.objects2 = new List<ObjectVM>();
            this.objects2.AddRange(objects1);
            //Object1
            this.Object1.DataSource = this.objects1;

            //Object2
            this.Object2.DataSource = this.objects2;

            //ConnectionTypeProperties
            if (ConnectionTypeList.SelectedItem != null)
            {
                this.ConnectionTypeProperties.DataSource = (ConnectionTypeList.SelectedItem as ConnectionTypeVM).ConnectionDatas;
            }
        }

        private void ConnectionTypeProperties_DoubleClick(object sender, EventArgs e)
        {
            if (this.ConnectionTypeProperties.SelectedItem != null)
            {
                EditProperty editProperty = new EditProperty(manager, this.ConnectionTypeProperties.SelectedItem as ConnectionDataVM);
                editProperty.Show();
            }
        }

        private void CreateConnectionButton_Click(object sender, EventArgs e)
        {
            if (this.Object1.SelectedItem != null && this.Object2.SelectedItem != null && this.ConnectionTypeList.SelectedItem != null)
            {
                this.manager.SaveConnection(this.Object1.SelectedValue.ToString(), this.Object2.SelectedValue.ToString(), this.ConnectionTypeList.SelectedValue.ToString());
                refreshObject1Connections();
                refreshObject2Connections();
            }
            else
            {
                MessageBox.Show("Error:\nSelect Object1 and Object2 and ConnectionType!");
            }
        }

        private void ConnectionTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ConnectionTypeList.SelectedItem != null)
            {
                this.ConnectionTypeProperties.DataSource = (ConnectionTypeList.SelectedItem as ConnectionTypeVM).ConnectionDatas;
            }
        }

        private void Object1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.Object1.SelectedItem != null)
            {
                refreshObject1Connections();
            }
        }

        private void Object2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.Object2.SelectedItem != null)
            {
                refreshObject2Connections();
            }
        }

        private void refreshObject1Connections()
        {
            this.Object1ExistingConnections.Clear();
            this.Object1ExistingConnections.View = View.Details;
            this.Object1ExistingConnections.Columns.Add("Object1");
            this.Object1ExistingConnections.Columns.Add("Object2");
            this.Object1ExistingConnections.Columns.Add("ConnectionType");
            this.Object1ExistingConnections.Columns.Add("Date");
            this.Object1ExistingConnections.Columns.Add("User");
            List<ConnectionVM> objectConnections = manager.GetConnectionsById(this.Object1.SelectedValue.ToString());
            foreach (ConnectionVM objConn in objectConnections)
            {

                this.Object1ExistingConnections.Items.Add(new ListViewItem(new string[] {
                        this.objects1.Where(x=> x.DeviceId == objConn.Object1).Select(y=> y.DisplayName).FirstOrDefault(),
                        this.objects2.Where(x=> x.DeviceId == objConn.Object2).Select(y=> y.DisplayName).FirstOrDefault(),
                        this.connectionTypes.Where(x=> x.TypeId == objConn.ConnectionTypeId).Select(y=> y.TypeName).FirstOrDefault(),
                        objConn.Date.HasValue ? objConn.Date.ToString() : "",
                        objConn.User
                    }));
            };
            if (objectConnections.Count > 0)
            {
                this.Object1ExistingConnections.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            }
            else
            {
                this.Object1ExistingConnections.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            this.Object1ExistingConnections.FullRowSelect = true;
        }

        private void refreshObject2Connections()
        {
            this.Object2ExistingConnections.Clear();
            this.Object2ExistingConnections.View = View.Details;
            this.Object2ExistingConnections.Columns.Add("Object1");
            this.Object2ExistingConnections.Columns.Add("Object2");
            this.Object2ExistingConnections.Columns.Add("ConnectionType");
            this.Object2ExistingConnections.Columns.Add("Date");
            this.Object2ExistingConnections.Columns.Add("User");

            List<ConnectionVM> objectConnections = manager.GetConnectionsById(this.Object2.SelectedValue.ToString());
            foreach (ConnectionVM objConn in objectConnections)
            {

                this.Object2ExistingConnections.Items.Add(new ListViewItem(new string[] {
                        this.objects2.Where(x=> x.DeviceId == objConn.Object1).Select(y=> y.DisplayName).FirstOrDefault(),
                        this.objects1.Where(x=> x.DeviceId == objConn.Object2).Select(y=> y.DisplayName).FirstOrDefault(),
                        this.connectionTypes.Where(x=> x.TypeId == objConn.ConnectionTypeId).Select(y=> y.TypeName).FirstOrDefault(),
                        objConn.Date.HasValue ? objConn.Date.ToString() : "",
                        objConn.User

                    }));
            };
            if (objectConnections.Count > 0)
            {
                this.Object2ExistingConnections.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            }
            else
            {
                this.Object2ExistingConnections.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            this.Object2ExistingConnections.FullRowSelect = true;
        }
    }
}
