﻿using BLL;
using BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Osszerendelo_modul
{
    public partial class ConnectionTypeAdd : Form
    {
        ConnectionTypeVM connectionType;
        Manager manager;
        public ConnectionTypeAdd(Manager manager)
        {
            InitializeComponent();
            this.manager = manager;
            connectionType = new ConnectionTypeVM();
            connectionType.ConnectionDatas = new List<ConnectionDataVM>();
            this.Properties.DataSource = connectionType.ConnectionDatas;
            ActiveControl = connectionTypeAddID;
        }

        public ConnectionTypeAdd(Manager manager, ConnectionTypeVM connectionType)
        {
            InitializeComponent();
            this.manager = manager;
            this.connectionType = connectionType;
            this.connectionType.ConnectionDatas = new List<ConnectionDataVM>(manager.GetConnectionTypeProperties(connectionType.TypeId));
            this.connectionTypeAddID.Text = connectionType.TypeId;
            this.connectionTypeAddName.Text = connectionType.TypeName;
            this.Properties.DataSource = this.connectionType.ConnectionDatas;
            ActiveControl = connectionTypeAddID;
        }

        private void PropertyAddButton_Click(object sender, EventArgs e)
        {
            connectionType.ConnectionDatas.Add(new ConnectionDataVM() {
                ConnectionDataName = this.connectionTypeAddPropertyName.Text,
            });
            this.connectionTypeAddPropertyName.Clear();
            this.Properties.DataSource = new List<ConnectionDataVM>(connectionType.ConnectionDatas);
        }

        private void PropertyDeleteButton_Click(object sender, EventArgs e)
        {
            if (this.Properties.SelectedItem != null)
            {
                connectionType.ConnectionDatas.Remove(connectionType.ConnectionDatas[this.Properties.SelectedIndex]);
                this.Properties.DataSource =  new List<ConnectionDataVM>(connectionType.ConnectionDatas);
            }
            
        }

        private void ConnectionAddCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConnectionAddSaveButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.connectionTypeAddID.Text) || string.IsNullOrEmpty(this.connectionTypeAddName.Text))
            {
                MessageBox.Show("Error: \nConnection Type ID and Connection Type Name are Required fields!");
            }
            else
            {
                this.connectionType.TypeId = this.connectionTypeAddID.Text;
                this.connectionType.TypeName = this.connectionTypeAddName.Text;
                foreach (ConnectionDataVM item in this.connectionType.ConnectionDatas)
                {
                    item.ConnectionTypeId = this.connectionType.TypeId;
                }
                
                manager.SaveConnectionType(this.connectionType);
                this.Close();
            }
        }
    }
}
