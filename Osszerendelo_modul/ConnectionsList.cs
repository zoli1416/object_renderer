﻿using BLL;
using BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListView;

namespace Osszerendelo_modul
{
    public partial class ConnectionsList : Form
    {
        Manager manager;
        List<ConnectionVM> objectConnections;
        public ConnectionsList(Manager manager)
        {
            InitializeComponent();
            this.manager = manager;
            refreshExistingConnections();
        }

        private void refreshExistingConnections()
        {
            this.ExistingConnections.Clear();
            this.ExistingConnections.View = View.Details;
            this.ExistingConnections.Columns.Add("Object1");
            this.ExistingConnections.Columns.Add("Object2");
            this.ExistingConnections.Columns.Add("ConnectionType");
            this.ExistingConnections.Columns.Add("Date");
            this.ExistingConnections.Columns.Add("User");
            this.objectConnections = manager.GetConnectionsForDisplay();
            foreach (ConnectionVM objConn in objectConnections)
            {
                this.ExistingConnections.Items.Add(new ListViewItem(new string[] {
                        objConn.Object1Display,
                        objConn.Object2Display,
                        objConn.ConnectionTypeIdDisplay,
                        objConn.Date.HasValue ? objConn.Date.ToString() : "",
                        objConn.User,
                    }));
            };
            if (objectConnections.Count > 0)
            {
                this.ExistingConnections.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            }
            else
            {
                this.ExistingConnections.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            this.ExistingConnections.FullRowSelect = true;
        }

        private void DeleteConnectionButton_Click(object sender, EventArgs e)
        {
            if (this.ExistingConnections.SelectedItems.Count > 0 )
            {
                foreach (ListViewItem item in this.ExistingConnections.SelectedItems)
                {
                    manager.DeleteConnection(this.objectConnections[item.Index].Object1, this.objectConnections[item.Index].Object2, this.objectConnections[item.Index].ConnectionTypeId);
                }
                refreshExistingConnections();
            }
        }
    }
}
