﻿using BLL;
using BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Osszerendelo_modul
{
    public partial class EditProperty : Form
    {
        Manager manager;
        ConnectionDataVM connectionDataVM;
        public EditProperty()
        {
            InitializeComponent();
        }

        public EditProperty(Manager manager, ConnectionDataVM connectionDataVM)
        {
            InitializeComponent();
            this.manager = manager;
            this.connectionDataVM = connectionDataVM;
            label1.Text = connectionDataVM.ConnectionDataName;
            EditPropertyValue.Text = connectionDataVM.ConnectionData;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.connectionDataVM.ConnectionData = EditPropertyValue.Text;
            this.manager.SaveConnectionData(this.connectionDataVM);
            this.Close();
        }
    }
}
