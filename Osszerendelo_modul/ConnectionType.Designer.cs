﻿namespace Osszerendelo_modul
{
    partial class ConnectionType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.connectionTypeAddButton = new System.Windows.Forms.Button();
            this.connectionTypeEditButton = new System.Windows.Forms.Button();
            this.connectionTypeDeleteButton = new System.Windows.Forms.Button();
            this.ConnectionTypes = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.connectionTypeAddButton);
            this.panel1.Controls.Add(this.connectionTypeEditButton);
            this.panel1.Controls.Add(this.connectionTypeDeleteButton);
            this.panel1.Controls.Add(this.ConnectionTypes);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(801, 451);
            this.panel1.TabIndex = 4;
            // 
            // connectionTypeAddButton
            // 
            this.connectionTypeAddButton.Location = new System.Drawing.Point(393, 39);
            this.connectionTypeAddButton.Name = "connectionTypeAddButton";
            this.connectionTypeAddButton.Size = new System.Drawing.Size(105, 38);
            this.connectionTypeAddButton.TabIndex = 6;
            this.connectionTypeAddButton.Text = "Add";
            this.connectionTypeAddButton.UseVisualStyleBackColor = true;
            this.connectionTypeAddButton.Click += new System.EventHandler(this.ConnectionTypeAddButton_Click);
            // 
            // connectionTypeEditButton
            // 
            this.connectionTypeEditButton.Location = new System.Drawing.Point(393, 98);
            this.connectionTypeEditButton.Name = "connectionTypeEditButton";
            this.connectionTypeEditButton.Size = new System.Drawing.Size(105, 38);
            this.connectionTypeEditButton.TabIndex = 5;
            this.connectionTypeEditButton.Text = "Edit";
            this.connectionTypeEditButton.UseVisualStyleBackColor = true;
            this.connectionTypeEditButton.Click += new System.EventHandler(this.ConnectionTypeEditButton_Click);
            // 
            // connectionTypeDeleteButton
            // 
            this.connectionTypeDeleteButton.Location = new System.Drawing.Point(393, 157);
            this.connectionTypeDeleteButton.Name = "connectionTypeDeleteButton";
            this.connectionTypeDeleteButton.Size = new System.Drawing.Size(105, 38);
            this.connectionTypeDeleteButton.TabIndex = 4;
            this.connectionTypeDeleteButton.Text = "Delete";
            this.connectionTypeDeleteButton.UseVisualStyleBackColor = true;
            this.connectionTypeDeleteButton.Click += new System.EventHandler(this.ConnectionTypeDeleteButton_Click);
            // 
            // ConnectionTypes
            // 
            this.ConnectionTypes.DisplayMember = "TypeName";
            this.ConnectionTypes.FormattingEnabled = true;
            this.ConnectionTypes.Location = new System.Drawing.Point(18, 14);
            this.ConnectionTypes.Name = "ConnectionTypes";
            this.ConnectionTypes.Size = new System.Drawing.Size(369, 342);
            this.ConnectionTypes.TabIndex = 1;
            this.ConnectionTypes.ValueMember = "TypeId";
            // 
            // ConnectionType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ConnectionType";
            this.Text = "Connection Types";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button connectionTypeAddButton;
        private System.Windows.Forms.Button connectionTypeEditButton;
        private System.Windows.Forms.Button connectionTypeDeleteButton;
        private System.Windows.Forms.ListBox ConnectionTypes;
    }
}