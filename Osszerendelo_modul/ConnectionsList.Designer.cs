﻿namespace Osszerendelo_modul
{
    partial class ConnectionsList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ExistingConnections = new System.Windows.Forms.ListView();
            this.DeleteConnectionButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(347, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(214, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Existing Connections";
            // 
            // ExistingConnections
            // 
            this.ExistingConnections.Location = new System.Drawing.Point(41, 138);
            this.ExistingConnections.Name = "ExistingConnections";
            this.ExistingConnections.Size = new System.Drawing.Size(795, 453);
            this.ExistingConnections.TabIndex = 14;
            this.ExistingConnections.UseCompatibleStateImageBehavior = false;
            // 
            // DeleteConnectionButton
            // 
            this.DeleteConnectionButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DeleteConnectionButton.Location = new System.Drawing.Point(721, 597);
            this.DeleteConnectionButton.Name = "DeleteConnectionButton";
            this.DeleteConnectionButton.Size = new System.Drawing.Size(115, 36);
            this.DeleteConnectionButton.TabIndex = 15;
            this.DeleteConnectionButton.Text = "Delete";
            this.DeleteConnectionButton.UseVisualStyleBackColor = true;
            this.DeleteConnectionButton.Click += new System.EventHandler(this.DeleteConnectionButton_Click);
            // 
            // ConnectionsList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 655);
            this.Controls.Add(this.DeleteConnectionButton);
            this.Controls.Add(this.ExistingConnections);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ConnectionsList";
            this.Text = "ConnectionsList";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView ExistingConnections;
        private System.Windows.Forms.Button DeleteConnectionButton;
    }
}