﻿namespace Osszerendelo_modul
{
    partial class ConnectionTypeAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.connectionTypeAddName = new System.Windows.Forms.TextBox();
            this.Properties = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.connectionTypeAddPropertyName = new System.Windows.Forms.TextBox();
            this.propertyAddButton = new System.Windows.Forms.Button();
            this.propertyDeleteButton = new System.Windows.Forms.Button();
            this.connectionAddCancelButton = new System.Windows.Forms.Button();
            this.connectionAddSaveButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.connectionTypeAddID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Connection Type Name";
            // 
            // connectionTypeAddName
            // 
            this.connectionTypeAddName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connectionTypeAddName.Location = new System.Drawing.Point(259, 71);
            this.connectionTypeAddName.Name = "connectionTypeAddName";
            this.connectionTypeAddName.Size = new System.Drawing.Size(255, 29);
            this.connectionTypeAddName.TabIndex = 1;
            // 
            // Properties
            // 
            this.Properties.DisplayMember = "ConnectionDataName";
            this.Properties.FormattingEnabled = true;
            this.Properties.Location = new System.Drawing.Point(16, 232);
            this.Properties.Name = "Properties";
            this.Properties.Size = new System.Drawing.Size(372, 199);
            this.Properties.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 192);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Property Name";
            // 
            // connectionTypeAddPropertyName
            // 
            this.connectionTypeAddPropertyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connectionTypeAddPropertyName.Location = new System.Drawing.Point(133, 183);
            this.connectionTypeAddPropertyName.Name = "connectionTypeAddPropertyName";
            this.connectionTypeAddPropertyName.Size = new System.Drawing.Size(176, 29);
            this.connectionTypeAddPropertyName.TabIndex = 4;
            // 
            // propertyAddButton
            // 
            this.propertyAddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.propertyAddButton.Location = new System.Drawing.Point(315, 183);
            this.propertyAddButton.Name = "propertyAddButton";
            this.propertyAddButton.Size = new System.Drawing.Size(75, 29);
            this.propertyAddButton.TabIndex = 5;
            this.propertyAddButton.Text = "Add";
            this.propertyAddButton.UseVisualStyleBackColor = true;
            this.propertyAddButton.Click += new System.EventHandler(this.PropertyAddButton_Click);
            // 
            // propertyDeleteButton
            // 
            this.propertyDeleteButton.Location = new System.Drawing.Point(394, 232);
            this.propertyDeleteButton.Name = "propertyDeleteButton";
            this.propertyDeleteButton.Size = new System.Drawing.Size(75, 23);
            this.propertyDeleteButton.TabIndex = 6;
            this.propertyDeleteButton.Text = "Delete";
            this.propertyDeleteButton.UseVisualStyleBackColor = true;
            this.propertyDeleteButton.Click += new System.EventHandler(this.PropertyDeleteButton_Click);
            // 
            // connectionAddCancelButton
            // 
            this.connectionAddCancelButton.Location = new System.Drawing.Point(627, 408);
            this.connectionAddCancelButton.Name = "connectionAddCancelButton";
            this.connectionAddCancelButton.Size = new System.Drawing.Size(75, 23);
            this.connectionAddCancelButton.TabIndex = 7;
            this.connectionAddCancelButton.Text = "Cancel";
            this.connectionAddCancelButton.UseVisualStyleBackColor = true;
            this.connectionAddCancelButton.Click += new System.EventHandler(this.ConnectionAddCancelButton_Click);
            // 
            // connectionAddSaveButton
            // 
            this.connectionAddSaveButton.Location = new System.Drawing.Point(708, 408);
            this.connectionAddSaveButton.Name = "connectionAddSaveButton";
            this.connectionAddSaveButton.Size = new System.Drawing.Size(75, 23);
            this.connectionAddSaveButton.TabIndex = 8;
            this.connectionAddSaveButton.Text = "Save";
            this.connectionAddSaveButton.UseVisualStyleBackColor = true;
            this.connectionAddSaveButton.Click += new System.EventHandler(this.ConnectionAddSaveButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "Connection Type ID";
            // 
            // connectionTypeAddID
            // 
            this.connectionTypeAddID.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connectionTypeAddID.Location = new System.Drawing.Point(259, 20);
            this.connectionTypeAddID.Name = "connectionTypeAddID";
            this.connectionTypeAddID.Size = new System.Drawing.Size(255, 29);
            this.connectionTypeAddID.TabIndex = 10;
            // 
            // ConnectionTypeAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.connectionTypeAddID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.connectionAddSaveButton);
            this.Controls.Add(this.connectionAddCancelButton);
            this.Controls.Add(this.propertyDeleteButton);
            this.Controls.Add(this.propertyAddButton);
            this.Controls.Add(this.connectionTypeAddPropertyName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Properties);
            this.Controls.Add(this.connectionTypeAddName);
            this.Controls.Add(this.label1);
            this.Name = "ConnectionTypeAdd";
            this.Text = "ConnectionTypeAdd";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox connectionTypeAddName;
        private System.Windows.Forms.ListBox Properties;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox connectionTypeAddPropertyName;
        private System.Windows.Forms.Button propertyAddButton;
        private System.Windows.Forms.Button propertyDeleteButton;
        private System.Windows.Forms.Button connectionAddCancelButton;
        private System.Windows.Forms.Button connectionAddSaveButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox connectionTypeAddID;
    }
}