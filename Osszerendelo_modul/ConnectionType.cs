﻿using BLL;
using BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Osszerendelo_modul
{
    public partial class ConnectionType : Form
    {
        Manager manager;
        List<ConnectionTypeVM> connectionTypes;
        public ConnectionType(Manager manager)
        {
            InitializeComponent();
            this.manager = manager;
            GetConnectionTypeData();
        }

        private void ConnectionTypeAddButton_Click(object sender, EventArgs e)
        {
            ConnectionTypeAdd connectionTypeAdd = new ConnectionTypeAdd(this.manager);
            connectionTypeAdd.Show();
            connectionTypeAdd.FormClosed += RefreshConnectionTypeListEvent;
        }

        private void RefreshConnectionTypeListEvent(object sender, FormClosedEventArgs e)
        {
            this.GetConnectionTypeData();
        }
        private void GetConnectionTypeData()
        {
            this.connectionTypes = manager.GetConnectionTypes();
            this.ConnectionTypes.DataSource = this.connectionTypes;
        }

        private void ConnectionTypeEditButton_Click(object sender, EventArgs e)
        {
            if (this.ConnectionTypes.SelectedItem != null)
            {
                ConnectionTypeAdd connectionTypeAdd = new ConnectionTypeAdd(this.manager, this.ConnectionTypes.SelectedItem as ConnectionTypeVM);
                connectionTypeAdd.Show();
                connectionTypeAdd.FormClosed += RefreshConnectionTypeListEvent;
            }
        }

        private void ConnectionTypeDeleteButton_Click(object sender, EventArgs e)
        {
            this.manager.DeleteConnectionType(this.ConnectionTypes.SelectedValue.ToString());
            GetConnectionTypeData();
        }
    }
}
