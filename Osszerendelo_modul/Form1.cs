﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Osszerendelo_modul
{
    public partial class Form1 : Form
    {
        Manager manager;
        public Form1()
        {
            InitializeComponent();
            manager = new Manager();
            Welcome welcome = new Welcome();
            nav(welcome, content);
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            Connections connections = new Connections(manager);
            nav(connections, content);
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            ConnectionType connectionType = new ConnectionType(manager);
            nav(connectionType, content);
        }
        private void nav(Form form, Panel panel)
        {
            form.TopLevel = false;
            panel.Controls.Clear();
            panel.Controls.Add(form);
            form.Show();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            ConnectionsList connectionsList = new ConnectionsList(manager);
            nav(connectionsList, content);
        }
    }
}
